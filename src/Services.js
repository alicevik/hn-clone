/**
 * Created by ali.cevik on 12/06/2017.
 */

import qs from "qs";
import { appConfig } from './config.js';


export default class APIService
{

    getSubmissionByID(id)
    {
        return fetch(appConfig.apiURL + '/submissions/' + id + '/').then( response => response.json());
    }

    getSubmissions()
    {
        return fetch(appConfig.apiURL + '/submissions/').then( response => response.json());
    }

    addSubmission(title, url)
    {
        let submission = {title:title, url:url};

        return fetch(appConfig.apiURL + '/submissions/',
            { method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'

                },
            body:qs.stringify(submission)});
    }

    addComment(submissionID, content)
    {
        let comment = {submission:submissionID, content:content};

        return fetch(appConfig.apiURL + '/comments/',
            { method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'

                },
                body:qs.stringify(comment)});
    }

    upvote(id)
    {
        let upvote = {submission: id};

        return fetch(appConfig.apiURL + '/upvotes/',
            { method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
                },
                body:qs.stringify(upvote)});
    }


}
