import React, { Component } from 'react';
import './App.css';
import "isomorphic-fetch"
import APIService from './Services.js';
import 'query-string'

import {Switch, Route} from 'react-router-dom'


class HNMain extends Component
{

    constructor(props, context)
    {
        super(props,context);
    }

    render()
    {
        return(
            <Switch>
                <Route exact path='/'>
                    <HNHome/>
                </Route>
                <Route exact path='/index'>

                    <HNHome/>
                </Route>
                <Route path='/submit' component={HNForm}/>
                <Route path='/submissions/:submissionID' component={HNSubmission}>

                </Route>
            </Switch>
        )
    }
}

class HNHome extends Component
{
    constructor(props,context)
    {
        super(props,context);

        this.state = {submissions: []};
        this.apiService = new APIService();
        this.apiService.getSubmissions().then(submissions => this.setState({submissions:submissions}));
        this.upvote = this.upvote.bind(this);
    }

    upvote(id)
    {
        this.apiService.upvote(id).then
        (
            this.apiService.getSubmissions().then(submissions => this.setState({submissions:submissions}))
        );
    }


    render()
    {
        const submissions = this.state.submissions.map((submission) =>
        {
            return <HNItem key={submission.id} upvote={() => this.upvote(submission.id)} submission={submission}/>
        });

        return (
            <table style={{border: '0px', cellPadding:'0px', cellSpacing:'0px'}} className="itemlist">
                <tbody>
                    {submissions}
                </tbody>
            </table>
        )
    }
}

class HNItem extends Component
{
    constructor(props,context)
    {
        super(props,context);
    }

    render()
    {

        let viewCommentsPage = "/submissions/" + this.props.submission.id;

        return (
            <div>
                <tr className="athing">
                    <td style={{align:'right', valign:'top'}} className="title">
                        {this.props.hideIndex ? <span className="rank"/> : <span className="rank">{this.props.submission.index}.</span>}
                    </td>
                    <td style={{valign:'top'}} className="votelinks">
                        {this.props.hideUpvote ? <center/> :
                        <center>
                            <a href="#" onClick={this.props.upvote}><div className="votearrow" title="upvote"/></a>
                        </center>}
                    </td>
                    <td className="title">
                        <a href={this.props.submission.url} className="storylink">{this.props.submission.title}</a>
                    </td>
                </tr>
                <tr>
                    <td colSpan="2"/>
                    <td className="subtext">
                        <span className="score">{this.props.submission.upvotes} points</span>
                        |
                        <a href={viewCommentsPage}>{this.props.submission.comment_count} comments</a>
                    </td>
                </tr>
                <tr className="spacer" style={{height: "5px"}}/>
            </div>

        )
    }
}




class HNForm extends Component
{
    constructor(props,context)
    {
        super(props,context);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {submitted:false, link:'http://'}
    }

    handleSubmit(event)
    {
        let service = new APIService();
        service.addSubmission(this.state.title, this.state.link).then(this.setState({submitted:true}));
        event.preventDefault();
    }

    handleChange(e)
    {
        this.setState({[e.target.name]: e.target.value});
    }

    render()
    {
        return (
            <tr>
                <td>
                {!this.state.submitted ?

                    <form onSubmit={this.handleSubmit}>

                        <table style={{border:'0px'}}>
                        <tbody>
                        <tr>
                            <td>title</td>
                            <td><input type="text" name="title" onChange={this.handleChange} size="50"/></td>
                        </tr>
                        <tr>
                            <td>url</td>
                            <td><input type="text" name="link" size="50" onChange={this.handleChange} value={this.state.link}/></td>
                        </tr>

                        <tr>
                            <td/>
                            <td>
                        <input type="submit" value="Submit"/>
                            </td>
                        </tr>
                        </tbody>
                        </table>

                    </form>
                    :
                    <tr>
                        <td> </td>
                        <td>
                            Your submission with title {this.state.title} has been submitted.
                        </td>
                    </tr>
                }
                </td>
            </tr>
        )
    }
}

class HNComment extends Component
{
    constructor(props,context)
    {
        super(props,context);
    }

    render()
    {
        return (
            <tr className="athing comtr">
                <td>
                    <table style={{border: '0px'}}>
                        <tbody>


                            <td className="default">

                                <div style={{marginTop:'2px', marginBottom:'-10px'}}>
                                    <span className="comhead">
                                        anonymous comment
                                    </span>
                                </div>

                                <br/>
                                <div className="comment">
                                    <span className="c00">
                                        {this.props.content}
                                    </span>
                                </div>
                            </td>
                        </tbody>
                    </table>
                </td>
            </tr>
        )
    }
}

class HNSubmission extends Component
{

    constructor(props,context)
    {
        super(props,context);
        console.log();

        this.state = {submission:{comments:[], url:'', title:''}};


        this.apiService = new APIService();
        this.apiService.getSubmissionByID(this.props.match.params.submissionID).then(submission =>
        {
            this.setState({submission: submission})
        });

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }

    handleChange(e)
    {
        this.setState({[e.target.name]: e.target.value});
    }


    handleSubmit(event)
    {
        event.preventDefault();
        this.apiService.addComment(this.state.submission.id, this.state.content ).then( () =>
        {
            window.location.reload();
        });
    }

    render()
    {

        const comments = this.state.submission.comments.map((comment, index) =>
        {
            return <HNComment key={index} content={comment.content}/>
        });

        return(
            <div>
                <HNItem hideIndex="true" hideUpvote="true" submission={this.state.submission}/>
                <div style={{height: '10px'}}/>
                <div>
                    <div>
                    <form onSubmit={this.handleSubmit}>
                    <textarea name="content" rows="6" cols="60" onChange={this.handleChange}/>

                        <br/>
                        <br/>
                    <input type="submit" value="add comment"/>

                    </form>
                    </div>
                </div>
                <br/>
                <br/>

                <table style={{border: '0px'}} className="comment-tree">
                    <tbody>
                        {comments}
                    </tbody>

                </table>
            </div>
        )
    }
}

class App extends Component
{

    render() {
    return (
        <center>
            <table id="hnmain" width="85%" cellPadding="0" cellSpacing="0" style={{paddingBottom:'0px', border: '0', backgroundColor: '#f6f6ef'}}>
                <tbody>
                    <tr>
                        <td style={{backgroundColor: '#ff6600'}}>
                            <table cellPadding="0" cellSpacing="0" style={{paddingBottom:'0px', border:'0', paddingTop: "2px", paddingLeft:"2px", width:'100%'}}>
                                <tbody>
                                    <tr>
                                        <td style={{width:'18px', paddingRight:'4px'}}>
                                            <a href="http://www.ycombinator.com">
                                                <img src="https://news.ycombinator.com/y18.gif" width="18" height="18" style={{borderColor:'#FFFFFF', borderWidth:'1px', borderStyle:'solid'}}/>
                                            </a>
                                        </td>

                                        <td style={{lineHeight:'12pt', height:'10px'}}>
                                            <span className="pagetop" >
                                                <b  className="hnname"><a href="/">Hacker News</a></b>

                                                <span style={{paddingLeft: '3px'}}>
                                                    <a href="/index">all</a>
                                                        {" "}|{" "}
                                                    <a href="/submit">submit</a>
                                                </span>

                                            </span>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>
                        </td>
                    </tr>
                    <tr style={{height: '10px'}}/>

                    <tr>
                        <td>
                            <HNMain/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </center>
    );
  }
}

export default App;
