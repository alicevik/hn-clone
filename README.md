# Simple Hacker News Clone
This is a simple HN Clone.

## Features
- You can submit an URI.
- You can comment under a submission.
- You can upvote submissions.


## Missing
- There's no registration or login. Everything is anonymous.
- You can upvote unlimited number of times since there's no login or session check.
- Only URL submissions accepted. (no **Show HN**s)
- You can't reply to or upvote comments.
- No input validation or error handling on the client side. Whatever you post/do, it'll give you a success message.

## How To Setup
Modify apiURL at src/config.js to point your own server Base URL.

## How To Run
This project was bootstrapped with Create React App. You can start it using npm.

## About The Code
Wanted to catch up with react & see how easy it would be to build an HN clone. This took about 8 hours in total.
I avoided investing to it though, didn't want to do Flux because I thought it would be an overkill, etc.

